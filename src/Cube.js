import Vector3 from './Vector3';
import { vec3, vec4, mat4 } from 'gl-matrix';


class Cube{
    constructor(size)
    {
        var side = size / 2;
   
        this.vertices = [
            // Front face
            -side, -side,  side,
             side, -side,  side,
             side,  side,  side,
            -side,  side,  side,
            
            // Back face
            -side, -side, -side,
            -side,  side, -side,
             side,  side, -side,
             side, -side, -side,
            
            // Top face
            -side,  side, -side,
            -side,  side,  side,
             side,  side,  side,
             side,  side, -side,
            
            // Bottom face
            -side, -side, -side,
             side, -side, -side,
             side, -side,  side,
            -side, -side,  side,
            
            // Right face
             side, -side, -side,
             side,  side, -side,
             side,  side,  side,
             side, -side,  side,
            
            // Left face
            -side, -side, -side,
            -side, -side,  side,
            -side,  side,  side,
            -side,  side, -side,
          ];

          this.indecies = [
            0,  1,  2,      0,  2,  3,    // front
            4,  5,  6,      4,  6,  7,    // back
            8,  9,  10,     8,  10, 11,   // top
            12, 13, 14,     12, 14, 15,   // bottom
            16, 17, 18,     16, 18, 19,   // right
            20, 21, 22,     20, 22, 23,   // left
          ]; 
    }
 
    drawcube(c, projection, mv) {
        for (var i = 0; i < this.indecies.length / 3; i++) {
            var t1 = this.indecies[i * 3 + 0] * 3;
            t1 = vec4(this.vertices[t1 + 0], this.vertices[t1 + 1], this.vertices[t1 + 2],1);
            
            var t2 = this.indecies[i * 3 + 1] * 3;
            t2 = vec4(this.vertices[t2 + 0], this.vertices[t2 + 1], this.vertices[t2 + 2],1);
            
            var t3 = this.indecies[i * 3 + 2] * 3;
            t3 = vec4(this.vertices[t3 + 0], this.vertices[t3 + 1], this.vertices[t3 + 2],1);
              
            c.drawTriangle(t1, t2, t3, projection, mv); 

        }
    }
}

export default Cube;
