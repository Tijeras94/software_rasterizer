import $ from 'jquery';

import {vec3, vec4, mat4} from 'gl-matrix';

class Canvas{
    constructor(selector, width, height){
        this.canvas = $('<canvas></canvas>').appendTo(selector);
        this.ctx = this.canvas[0].getContext('2d');   
        this.ctx.canvas.width = width;
        this.ctx.canvas.height = height; 
        this.w = width;
        this.h = height;
        this.buf = this.ctx.createImageData(this.w, this.h); 
        this.scanbuff = new Array(height * 2);
        this.log = $('<h1 style="    font-size: 10;font-style: normal; font-weight: normal;">CanvasLog: </h1>').appendTo('body');

    }

    setPixel (x, y, r, g, b, a = 255) {
        x = Math.round(x);
        y = Math.round(y);
        if(x >= this.w|| (x < 0))
            return;
        if(y >= this.h || (y < 0))
        return;
        
        var i = (x * 4) + (y * (this.w * 4));
        this.buf.data[i + 0] = Math.round(r);
        this.buf.data[i + 1] = Math.round(g);
        this.buf.data[i + 2] = Math.round(b);
        this.buf.data[i + 3] = Math.round(a);
    }

    draw(mouse = false){
        if(mouse)
            this.renderMouse(); 
        this.ctx.putImageData(this.buf, 0, 0); 
    }


    clear (r, g, b, a = 255) {
        for (var y = 0; y < this.h; y++) {
            for (var x = 0; x < this.w; x++) {
                 this.setPixel(x,y, r, g, b, a);
            }
    
        }
    }
    
    initMouse (){
           var that = this;
            document.onmousemove = function(e) {
                var event = e || window.event;
                var rect = that.canvas[0].getBoundingClientRect();  
                that.mouseX = Math.round(event.clientX - rect.left);
                that.mouseY = Math.round(event.clientY - rect.top); 
            }
    }
    
    renderMouse (){ 
        this.drawCube(this.mouseX,this.mouseY,10, 255, 255, 255);
    }
    
    drawCube (x,y,size, r,g,b,a = 255){
        for (var i =y  ; i < size + y ; i++) {
            for (var w =x; w < size + x; w++) {
                this.setPixel(w-(size/2),i-(size/2), r,g,b,a);
            }
        }  
    } 
    
    setLoop ( render) {
        var running, lastFrame = +new Date;
        var that = this;
        function loop( now ) {
            // stop the loop if render returned false
            if ( running !== false ) {
                requestAnimationFrame( loop );
                var deltaT = now - lastFrame;
                // do not render frame when deltaT is too high
                if ( deltaT < 160 ) { 
                    running = function (){ 
                        let out = render( deltaT );
                        that.draw(true); 
                        return out;
                    }();
                    
                }
                
                lastFrame = now;
            }
        }
        loop( lastFrame );
    }
    
    DrawLine(x1, y1, x2, y2) {
    
        // Iterators, counters required by algorithm
        let x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
    
        // Calculate line deltas
        dx = x2 - x1;
        dy = y2 - y1;
    
        // Create a positive copy of deltas (makes iterating easier)
        dx1 = Math.abs(dx);
        dy1 = Math.abs(dy);
    
        // Calculate error intervals for both axis
        px = 2 * dy1 - dx1;
        py = 2 * dx1 - dy1;
    
        // The line is X-axis dominant
        if (dy1 <= dx1) {
    
            // Line is drawn left to right
            if (dx >= 0) {
                x = x1; y = y1; xe = x2;
            } else { // Line is drawn right to left (swap ends)
                x = x2; y = y2; xe = x1;
            }
    
            this.setPixel(x, y, 255,255,255); // Draw first pixel
    
            // Rasterize the line
            for (i = 0; x < xe; i++) {
                x = x + 1;
    
                // Deal with octants...
                if (px < 0) {
                    px = px + 2 * dy1;
                } else {
                    if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0)) {
                        y = y + 1;
                    } else {
                        y = y - 1;
                    }
                    px = px + 2 * (dy1 - dx1);
                }
    
                // Draw pixel from line span at currently rasterized position
                this.setPixel(x, y, 255,255,255);
            }
    
        } else { // The line is Y-axis dominant
    
            // Line is drawn bottom to top
            if (dy >= 0) {
                x = x1; y = y1; ye = y2;
            } else { // Line is drawn top to bottom
                x = x2; y = y2; ye = y1;
            }
    
            this.setPixel(x, y, 255,255,255); // Draw first pixel
    
            // Rasterize the line
            for (i = 0; y < ye; i++) {
                y = y + 1;
    
                // Deal with octants...
                if (py <= 0) {
                    py = py + 2 * dx1;
                } else {
                    if ((dx < 0 && dy<0) || (dx > 0 && dy > 0)) {
                        x = x + 1;
                    } else {
                        x = x - 1;
                    }
                    py = py + 2 * (dx1 - dy1);
                }
    
                // Draw pixel from line span at currently rasterized position
                this.setPixel(x, y, 255,255,255);
            }
        }
     }

     cross(a, b, c) {
        return (b[0] - a[0]) * -(c[1] - a[1]) - -(b[1] - a[1]) * (c[0] - a[0]);
      }
     
     fillScanBuffer(ymin, ymax, v0, v1, v2)
     {      
        
  
          // precalculate the area of the parallelogram defined by our triangle
        var area = this.cross(v0, v1, v2);
        let p = {};

        for (let i = ymin; i <= ymax; i++) {  
            for (let xm = this.scanbuff[i*2]; xm <  this.scanbuff[i*2+1]; xm++) {
                p[0] = xm + 0.5;
                p[1] = i + 0.5;

                

                // calculate vertex weights
                // should divide these by area, but we do that later
                // so we divide once, not three times
                var w0 = this.cross(v1,  v2, p) / area;
                var w1 = this.cross(v2, v0, p) / area;
                var w2 = this.cross(v0, v1, p) / area;

                // if the point is not inside our polygon, skip fragment
                /// dont color pixel out side of range
                if (w0 < 0 || w1 < 0 || w2 < 0) {
                    continue;
                }
              
                let r = (w0 * v0.c.r + w1 * v1.c.r + w2 * v2.c.r) ;
                let g = (w0 * v0.c.g + w1 * v1.c.g  + w2 * v2.c.g ) ;
                let b = (w0 * v0.c.b + w1 * v1.c.b + w2 * v2.c.b) ;
                
                this.setPixel(xm, i, Math.round(r),Math.round(g),Math.round(b)); 

            }
        }
     }

     
     scanConverline(min, max, handness, wireframe =  false)
    {  
        // if y distance less than 0 , theres noting we can do , return
        if((max[1] - min[1]) <=0 )
            return;
        
        let slope = (max[0] - min[0])/(max[1] - min[1]);
        let curx = min[0]; 
        for (let j = min[1]; j < max[1]; j++) {  
            this.scanbuff[j*2 + handness] = Math.round(curx + 0.5);  // round current x to an integer

            if(handness == 0 && false)
                this.setPixel(Math.round(curx), j, 255,0,255); 
            if(handness == 1 && false)
                this.setPixel(Math.round(curx), j, 0,0,255);    

            //increment current x by slope
            curx += slope;
        }
    }
    
    toScreenSpace(vect)
    {
        vect = [...vect];
        vect[0] = Math.round((vect[0]/vect[3] + 1) *  this.w / 2);
        vect[1] = Math.round((-vect[1]/vect[3] + 1) * this.h / 2);  
        return vect;
    }

    getTriangleArea(that, b, c){
        let x1 = b[0] - that[0];
        let y1 = b[1] - that[1];

        let x2 = c[0] - that[0];
        let y2 = c[1] - that[1];

        return ((x1 * y2) - (x2 * y1));
    }

    drawTriangle(minY,midY,maxY, projection, mv){  

        minY = mat4(mv) *  vec4(minY[0], minY[1], minY[2], 1);  
        midY = mat4(mv) *  vec4(midY[0], midY[1], midY[2], 1);  
        maxY = mat4(mv) *  vec4(maxY[0], maxY[1], maxY[2], 1);  

        //Back-face culling
        let n1 = vec4.create();
        vec4.sub(n1,midY, minY );
        let n2 = vec4.create();
        vec4.subtract(n2,maxY, minY);
        let n= vec4.create();
        vec3.cross(n, n1, n2); 

        if(!(vec4.dot(n, minY) > 0.0))
        {
            //draw triangle is its visible
            this._drawNormalizeTriangle(mat4(projection) *   vec4(minY), mat4(projection) *  vec4(midY), mat4(projection) *  vec4(maxY));
        } 
     }


    _drawNormalizeTriangle(minY, midY, maxY) {
        minY = this.toScreenSpace(minY);
        midY = this.toScreenSpace(midY);
        maxY = this.toScreenSpace(maxY);

        minY.c = { r: 255, g: 0, b: 0 };
        midY.c = { r: 0, g: 255, b: 0 };
        maxY.c = { r: 0, g: 0, b: 255 };



        if (maxY[1] < midY[1])
            [maxY, midY] = [midY, maxY];
        if (midY[1] < minY[1])
            [minY, midY] = [midY, minY];
        if (maxY[1] < midY[1])
            [maxY, midY] = [midY, maxY];
        let handness = this.getTriangleArea(minY, maxY, midY) >= 0 ? 1 : 0;
        this.scanConverline(minY, maxY, 0 + handness);
        this.scanConverline(minY, midY, 1 - handness);
        this.scanConverline(midY, maxY, 1 - handness);
        this.fillScanBuffer(minY[1], maxY[1], minY, midY, maxY);
       // this.DrawLine(minY[0], minY[1], maxY[0], maxY[1]);
       // this.DrawLine(minY[0], minY[1], midY[0], midY[1]);
       // this.DrawLine(midY[0], midY[1], maxY[0], maxY[1]);
        return { minY, midY, maxY };
    }
}

export default Canvas;
