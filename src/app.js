import 'babel-polyfill';
import $ from 'jquery';
import Canvas from './Canvas';
import Cube from './Cube';
import {vec2, vec3, vec4, mat2, mat2d, mat3, mat4, quat, quat2} from 'gl-matrix';


// parses an obj file from a text string
function parseObj(text) {
    var verts = [];
    var faces = [];
    
    // split the text into lines
    var lines = text.replace('\r', '').split('\n');
    var count = lines.length;
    
    for (var i = 0; i < count; i++) {
      var line = lines[i];
      
      if (line[0] == 'v') {
        // lines that start with 'v' are vertices
        var tokens = line.split(' ');
        verts.push({
          x: parseFloat(tokens[1]), 
          y: parseFloat(tokens[2]), 
          z: parseFloat(tokens[3])
        });
      }
      else if (line[0] == 'f') {
        // lines that start with 'f' are faces
        var tokens = line.split(' ');
        var face = [
          parseInt(tokens[1], 10),
          parseInt(tokens[2], 10),
          parseInt(tokens[3], 10)
        ];
        faces.push(face);
        
        if (face[0] < 0) {
          face[0] = verts.length + face[0];
        }
        if (face[1] < 0) {
          face[1] = verts.length + face[1];
        }
        if (face[2] < 0) {
          face[2] = verts.length + face[2];
        }
      }
    }
    
    // return an object containing our vertices and faces
    return {
      verts: verts,
      faces: faces
    };
  }

$('<h1>Render</h1>').appendTo('body');
let log = $('<h1 style="    font-size: 10;font-style: normal; font-weight: normal;">Log: </h1>').appendTo('body');

let c = new Canvas('body', 600,400);
c.initMouse();
let cube = new Cube(1); 

var projection = mat4.create();
mat4.perspective( projection, 45 * Math.PI / 180, c.w/c.h, 1 , 10 );
//mat4.identity(projection);

var view = mat4.create();
mat4.identity(view);

let obj = parseObj($("#teapot").text().trim());
 
 
var rot = 0;
c.setLoop(function(delta){
    c.clear(87,87,87);

    let move =  ((c.mouseX / c.w) - 0.5);
    //log.text(move);
  
    var model = mat4.create();
    mat4.identity(model);

    rot += delta * 0.02;

    
    mat4.translate(model,model, vec3(0 , .5* Math.sin( 5* rot* Math.PI/ 180) - .60, -7));
    //mat4.rotate(model, model, -1 * rot * Math.PI/180, vec3(1,1,1));  
    //mat4.scale(model, model, vec3(.5,.5,.5));
    

    let mvp = mat4.create();
    let mv = mat4.create();
    mat4.mul(mv, view, model);
    mat4.mul(mvp, projection, mv );

    //cube.drawcube(c, projection, mv);

     // draw our model
    for (var i = 0; i < obj.faces.length; i++) {
        var face = obj.faces[i];
        var v0 = obj.verts[face[0] - 1];
        var v1 = obj.verts[face[1] - 1];
        var v2 = obj.verts[face[2] - 1];
        
        if (v0 && v1 && v2) {
             c.drawTriangle([v0.x, v0.y, v0.z], [v1.x, v1.y, v1.z], [v2.x, v2.y, v2.z], projection, mv);  
        }
        else {
        if (!v0) { console.log("Vertice " + (face[0] - 1) + " not found!"); }
        if (!v1) { console.log("Vertice " + (face[1] - 1) + " not found!"); }
        if (!v2) { console.log("Vertice " + (face[2] - 1) + " not found!"); }
        }
    }


    c.draw(true);

    //return false;
});
 





